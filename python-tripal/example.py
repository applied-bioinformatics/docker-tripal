from tripal import TripalInstance

ti = TripalInstance(tripal_url='http://localhost:3000/', username='admin', password='changeme')

# Create human species
#org = ti.organism.add_organism(genus="Solanum", species="lycopersicum", common="Tomato", abbr="SOLYC4.0")

# Then display the list of organisms
orgs = ti.organism.get_organisms()

for org in orgs:
    print('{} {}'.format(org['genus'], org['species']))

# Create an analysis
an = ti.analysis.add_analysis(name="Main assembly",
                              program="SolGenomicsNetwork assembler",
                              programversion="1.0",
                              algorithm="Unknown",
                              sourcename="src",
                              sourceversion="2.1beta",
                              date_executed="2020-09-20")

ans = ti.analysis.get_analyses()


# And load some data
ti.analysis.load_fasta(fasta="./data/S_lycopersicum_chromosomes.4.00.fa", analysis_id=ans[0]['analysis_id'], organism_id=orgs[0]['organism_id'])
ti.analysis.load_gff3(gff="./data/ITAG4.0_gene_models.gff", analysis_id=ans[0]['analysis_id'], organism_id=orgs[0]['organism_id'])

